[[_TOC_]]
# Introducción
Este repositorio aloja una práctica para observar el funcionamiento de un _sharding_ y replicaset de mongodb

# Prerequisitos
1. git
1. docker y docker-compose

# ¿Cómo usar este repositorio?
## Configurar replicaset y shards
1. En una terminal, clonar este repositorio:
```sh
git clone https://gitlab.com/posgrado/ceiot/big-data/sharding-en-mongodb.git
```
2. En la misma terminal, entrar al directorio e iniciar los contenedores:
```sh
cd sharding-en-mongodb
docker-compose up
```
3. En una segunda terminal, conectarse a una de las instancias y configuarar el replicaset para guardar datos:
```sh
cd sharding-en-mongodb
mongo --port 27020 --shell ./scripts/replicaset_data0.js
```
4. En una tercera terminal, conectarse a una de las instancias y configuarar el replicaset para guardar datos:
```sh
cd sharding-en-mongodb
mongo --port 27021 --shell ./scripts/replicaset_data1.js
```
5. En una cuarta terminal, conectarse a una de las instancias y configuarar el replicaset para guardar datos:
```sh
cd sharding-en-mongodb
mongo --port 27022 --shell ./scripts/replicaset_data2.js
```
6. En una quita terminal, conectarse a una de las instancias y configuarar el replicaset para configuración:
```sh
cd sharding-en-mongodb
mongo --port 27019 --shell ./scripts/replicaset_config.js
```
7. Una sexta terminal será usada para cargar datos dentro del resplicaset
```sh
cd sharding-en-mongodb
mongo --port 27017 --eval 'sh.addShard("data0_rs/data0_primary,data0_secondary")' --shell
```

## Prácticas con el clúster
1. La sexta terminal que usamos, ha quedado conectada a mongos, allí vamos a configurar los _shards_.
```js
use finanzas
//Creo el indice
db.facturas.createIndex({"cliente.region": 1, condPago: 1, _id: 1})
//Activo el Sharding para la base de datos finanzas
sh.enableSharding("finanzas")
//Shardeo la Colección facturas de la Base de datos Finanzas
sh.shardCollection("finanzas.facturas", {"cliente.region": 1, condPago: 1, _id: 1})
//Veo la metadata del cluster
sh.status()
//Veo los chunks que se crearon
use config
db.chunks.find({}, {min:1,max:1,shard:1,_id:0,ns:1}).pretty()
//Agrego 2 nuevos shards
sh.addShard("data1_rs/data1_primary,data1_secondary")
sh.addShard("data2_rs/data2_primary,data2_secondary")
//Cargo datos para verificar el funcionamiento
for (var i=0; i<5; i++) {
    load("facts.js")
}
//Veo la metadata del cluster
sh.status()
//Query que apunte a un grupo limitado de Shards
db.facturas.find({"cliente.region":"CABA", "condPago":"30 Ds FF"}).explain()
//Query que se dispara a todos los Shards
db.facturas.find({"cliente.apellido":"Manoni"}).explain()
```

## Video demostrativo
A continuación se puede ver el proceso anterior:
![](video/sharding_shell.mov)
