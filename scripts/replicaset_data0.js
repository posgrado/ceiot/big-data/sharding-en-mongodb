cfg = {
    _id:"data0_rs",
    members:[
        {_id:0, host:"data0_primary:27017"},
        {_id:1, host:"data0_secondary:27017"},
        {_id:2, host:"data0_arbiter:27017", arbiterOnly:true}
    ]
}
rs.initiate(cfg)
rs.status()