cfg = {
    _id:"data2_rs",
    members:[
        {_id:0, host:"data2_primary:27017"},
        {_id:1, host:"data2_secondary:27017"},
        {_id:2, host:"data2_arbiter:27017", arbiterOnly:true}
    ]
}
rs.initiate(cfg)
rs.status()