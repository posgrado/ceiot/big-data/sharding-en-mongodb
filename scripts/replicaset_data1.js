cfg = {
    _id:"data1_rs",
    members:[
        {_id:0, host:"data1_primary:27017"},
        {_id:1, host:"data1_secondary:27017"},
        {_id:2, host:"data1_arbiter:27017", arbiterOnly:true}
    ]
}
rs.initiate(cfg)
rs.status()